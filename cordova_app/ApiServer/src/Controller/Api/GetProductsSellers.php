<?php

namespace App\Controller\Api;

use App\Entity\Seller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GetProductsSellers
{
    public function __construct(EntityManagerInterface $em, NormalizerInterface $normalizer) 
    {
        $this->em = $em;
        $this->normalizer = $normalizer;
    }

    public function __invoke(Seller $data) 
    {
        $products = $this->normalizer->normalize($data->getProducts(), null, ['groups' => 'sellerprod:read']);
        return [
            "message" => "Voici les produits du vendeur '".$data->getCompanyName(). ", id=". $data->getId(),
            "products"  => $products
        ]; 
    }

}