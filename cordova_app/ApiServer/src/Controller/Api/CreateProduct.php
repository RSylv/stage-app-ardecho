<?php

namespace App\Controller\Api;

use DateTime;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class CreateProduct 
{
    protected $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function  __invoke(Product $data)
    {
        $data->setCreatedAt(new DateTime());
    }
}