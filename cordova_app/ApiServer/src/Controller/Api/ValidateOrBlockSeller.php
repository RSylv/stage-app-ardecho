<?php

namespace App\Controller\Api;

use App\Entity\Seller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ValidateOrBlockSeller
{
    protected $em;

    public function __construct(EntityManagerInterface $em, NormalizerInterface $normalizer) 
    {
        $this->em = $em;
    }

    public function __invoke(Seller $data) 
    {
        if (!$data->getIsValidate()) 
        {   
            $data->setIsValidate(true);
            $this->em->persist($data);
            $this->em->flush();
    
            $value = [
                "message" => "Le vendeur '".$data->getCompanyName()."' a été validé !.",
                "isValidate" => $data->getIsValidate()
            ];
            return new JsonResponse($value, 200);
        }
        if ($data->getIsValidate()) 
        {
            $data->setIsValidate(false);
            $this->em->persist($data);
            $this->em->flush();
    
            $value = [
                    "message"  => "Le vendeur '".$data->getCompanyName()."' a été bloqué !.",
                    "isValidate" => $data->getIsValidate()
                ];
            return new JsonResponse($value, 200);
        }
        else
        {
            throw new BadRequestException("Cette action n'est pas autorisée ici.", 401);            
        }
    }
}