<?php

namespace App\Controller\Api;

use App\Entity\Seller;
use Doctrine\ORM\EntityManagerInterface;

class GetPremiumSellers
{
    public function __construct(EntityManagerInterface $em) 
    {
        $this->em = $em;
    }

    public function __invoke() 
    {
        return $this->em->getRepository(Seller::class)
            ->getPremiumSellers();
    }

}