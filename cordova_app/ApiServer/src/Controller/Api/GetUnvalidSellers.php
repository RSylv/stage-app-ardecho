<?php

namespace App\Controller\Api;

use App\Entity\Seller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GetUnvalidSellers
{
    public function __construct(EntityManagerInterface $em, NormalizerInterface $normalizer) 
    {
        $this->em = $em;
        $this->normalizer = $normalizer;
    }

    public function __invoke() 
    {
        $sellers = $this->em->getRepository(Seller::class)->getUnValidSellers();
        
        $data = $this->normalizer->normalize($sellers);

        $response = new JsonResponse($data, 200, ['groups' => 'seller:read']);
        return $response;
    }

}
