<?php

namespace App\Controller\Api;

use App\Entity\Category;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GetSubCategories
{
    public function __construct(NormalizerInterface $normalizer) {
        $this->normalizer = $normalizer;
    }

    public function __invoke(Category $data)
    {
        $sub = $this->normalizer->normalize($data->getSubCategories());
        return new JsonResponse($sub, 200, ['groups' => 'cat:read']);
    }
}
