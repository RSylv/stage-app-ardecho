<?php

namespace App\Controller\Api;

use App\Entity\SubCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class GetProductsOfSubCategory 
{
    private $em;

    public function __construct(EntityManagerInterface $em, NormalizerInterface $normalizer) 
    {
        $this->em = $em;
        $this->normalizer = $normalizer;
    }

    public function __invoke(SubCategory $data) 
    {
        $products = $this->normalizer->normalize($data->getProducts(), null, ['groups' => 'subProd:read']);
        return new JsonResponse($products, 200);
    }
}