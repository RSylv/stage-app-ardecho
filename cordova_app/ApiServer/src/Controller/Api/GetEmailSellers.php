<?php

namespace App\Controller\Api;

use App\Entity\Seller;
use Doctrine\ORM\EntityManagerInterface;

class GetEmailSellers
{
    public function __construct(EntityManagerInterface $em) 
    {
        $this->em = $em;
    }

    public function __invoke() 
    {
        return $this->em->getRepository(Seller::class)
            ->getEmails();
    }

}
