<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Entity\Seller;
use App\Manager\UserManager;
use App\Manager\SellerManager;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CreateAccount
{
    protected $userManager;
    protected $sellerManager;

    public function __construct(UserManager $userManager, SellerManager $sellerManager)
    {
        $this->userManager = $userManager;
        $this->sellerManager = $sellerManager;
    }

    public function __invoke($data)
    {
        if ($data instanceof User) 
        {
            $this->userManager->registerAccount($data);
            return $data;
        } 
        else if ($data instanceof Seller)
        {
            $this->sellerManager->registerAccount($data);
            return $data;
        }
        else {
            throw new BadRequestException("Vous n'êtes pas autorisé a envoyer ce genre de données ici.", 401);
        }
    }
}
