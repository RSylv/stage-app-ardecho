<?php

namespace App\Controller\Api;

use App\Entity\Seller;
use Doctrine\ORM\EntityManagerInterface;

class GetAddress 
{
    protected $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function __invoke(Seller $data)
    {
        $id = $data->getId();
        return $this->em->getRepository(Seller::class)
            ->getAddress($id);
    }
}