<?php

namespace App\Repository;

use App\Entity\Seller;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use PhpParser\Node\Expr\Cast\Int_;

/**
 * @method Seller|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seller|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seller[]    findAll()
 * @method Seller[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SellerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Seller::class);
    }

    public function getEmails() 
    {
        return $this->createQueryBuilder('u')
            ->select('u.email, u.phone')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getAddress($id)
    {
        return $this->createQueryBuilder('u')
            ->select('u.name, u.surname, u.companyName, u.address, u.zipCode, u.city')
            ->andWhere('u.id = :id')
            ->setParameter('id', $id)
            ->orderBy('u.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByDepartmentCode($value)
    {
        return $this->createQueryBuilder('u')
            ->select('u.name, u.surname, u.companyName, u.address, u.zipCode, u.city')
            ->andWhere('u.zipCode LIKE :val')
            ->setParameter('val', $value.'%')
            ->orderBy('u.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getValidSellers()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.isValidate = :val')
            ->setParameter('val', true)
            ->orderBy('u.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;

    }

    public function getUnvalidSellers()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.isValidate = :val')
            ->setParameter('val', false)
            ->orderBy('u.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;

    }

    public function getPremiumSellers()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.accountType = :val')
            ->setParameter('val', sprintf('%s','PREMIUM'))
            ->orderBy('u.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;

    }

    // /**
    //  * @return Seller[] Returns an array of Seller objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Seller
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
