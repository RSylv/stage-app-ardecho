<?php

namespace App\Event\Listener;

use App\Entity\Seller;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{
    /**
     * Adds additional data to the generated JWT
     * 
     * @param AuthenticationSuccessListener $event
     * 
     * @return void 
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        else if ($user instanceof User) {
            $data['data'] = array(
                'id'          => $user->getId(),
                'username'    => $user->getUsername(),
                'roles'       => $user->getRoles(),
                'email'       => $user->getEmail(),
                'name'        => $user->getName(),
                'surname'     => $user->getSurname(),
                'phone'       => $user->getPhone(),
                'address'     => $user->getAddress(),
                'city'        => $user->getCity(),
                'zipCode'     => $user->getZipCode(),
                'createdAt'   => $user->getCreatedAt(),    
            );
        }

        else if ($user instanceof Seller) {
            $data['data'] = array(
                'id'          => $user->getId(),
                'username'    => $user->getUsername(),
                'roles'       => $user->getRoles(),
                'email'       => $user->getEmail(),
                'companyName' => $user->getCompanyName(),
                'accountType' => $user->getAccountType(),
                'name'        => $user->getName(),
                'surname'     => $user->getSurname(),
                'phone'       => $user->getPhone(),
                'address'     => $user->getAddress(),
                'city'        => $user->getCity(),
                'zipCode'     => $user->getZipCode(),
                'createdAt'   => $user->getCreatedAt(),    
            );
        }

        $event->setData($data);

    }
}
