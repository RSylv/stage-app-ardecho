<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SellerRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"createdAt":"DESC"}
 *      },
 *      normalizationContext={"groups": {"seller:read", "sellerprod:read"}},
 *      denormalizationContext={"groups": {"seller:write"}},
 *      collectionOperations={
 *          "get"={"controller"="App\Controller\Api\GetValidSellers"},
 *          "post"={
 *              "path"="/sellers/create",
 *              "controller"="App\Controller\Api\CreateAccount",
 *          },
 *          "emails_sellers"={
 *              "method"="GET",
 *              "path"="/auth/sellers/emails",
 *              "controller"="App\Controller\Api\GetEmailSellers",
 *          },
 *          "valid_sellers"={
 *              "method"="GET",
 *              "path"="/auth/sellers/unvalidate",
 *              "controller"="App\Controller\Api\GetUnvalidSellers",
 *          },
 *          "premium_sellers"={
 *              "method"="GET",
 *              "path"="/auth/sellers/premium",
 *              "controller"="App\Controller\Api\GetPremiumSellers",
 *          },  *           
 *      },      
 *      itemOperations={
 *          "get"={},  
 *          "address_sellers"={
 *              "method"="GET",
 *              "path"="/auth/sellers/{id}/address",
 *              "controller"="App\Controller\Api\GetAddress",
 *          },
 *          "products_sellers"={
 *              "method"="GET",
 *              "path"="/sellers/{id}/products",
 *              "controller"="App\Controller\Api\GetProductsSellers",
 *          },
 *          "put"={},
 *          "patch"={},
 *          "validate_a_sellers"={
 *              "method"="GET",
 *              "path"="/auth/sellers/{id}/validate",
 *              "controller"="App\Controller\Api\ValidateOrBlockSeller",
 *          },
 *          "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *      }
 * )
 * @UniqueEntity("email")
 * @ORM\Entity(repositoryClass=SellerRepository::class)
 */
class Seller implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"seller:read", "product:read", "subProd:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"seller:read", "seller:write"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"seller:read"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"seller:write"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=14)
     * @Groups({"seller:read", "seller:write"})
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=80)
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=80)
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $surname;

    /**
     * @ORM\Column(type="text")
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=5)
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $zipCode;

    /**
     * @ORM\Column(type="text")
     * @Groups({"seller:read", "seller:write", "subProd:read"})
     */
    private $openingHours;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"seller:read"})
     */
    private $isValidate;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"seller:read"})
     */
    private $accountType;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"seller:read", "subProd:read"})
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="seller", orphanRemoval=true)
     * @Groups({"sellerprod:read"})
     */
    private $products;


    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getOpeningHours(): ?string
    {
        return $this->openingHours;
    }

    public function setOpeningHours(string $openingHours): self
    {
        $this->openingHours = $openingHours;

        return $this;
    }

    public function getIsValidate(): ?bool
    {
        return $this->isValidate;
    }

    public function setIsValidate(bool $isValidate): self
    {
        $this->isValidate = $isValidate;

        return $this;
    }

    public function getAccountType(): ?string
    {
        return $this->accountType;
    }

    public function setAccountType(string $accountType): self
    {
        $this->accountType = $accountType;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setSeller($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getSeller() === $this) {
                $product->setSeller(null);
            }
        }

        return $this;
    }

}
