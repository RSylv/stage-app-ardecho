<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProductRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(      
 *      normalizationContext={"groups": {"product:read"}},
 *      denormalizationContext={"groups": {"product:write"}},
 *      collectionOperations={
 *          "get"={},
 *          "post"={},
 *      },
 *      itemOperations={
 *          "get"={},
 *          "patch"={},
 *          "delete"={}
 *      },
 * )
 * @ApiFilter(SearchFilter::class, properties={"title": "exact", "description": "partial"})
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"product:read", "sellerprod:read", "subProd:read"})
     */
    private  $id;

    /**
     * @ORM\Column(type="string", length=160)
     * @Groups({"product:read", "product:write", "sellerprod:read", "subProd:read"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"product:read", "product:write", "sellerprod:read", "subProd:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"product:read", "product:write", "sellerprod:read", "subProd:read"})
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"product:read", "product:write", "sellerprod:read", "subProd:read"})
     */
    private $stock;

    /**
     * @ORM\ManyToOne(targetEntity=SubCategory::class, inversedBy="products", cascade="persist")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"product:read", "product:write"})
     */
    private $subCategory;

    /**
     * @ORM\ManyToOne(targetEntity=Seller::class, inversedBy="products", cascade="persist")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"product:read", "product:write", "subProd:read"})
     */
    private $seller;

    /**
     * @ORM\OneToMany(targetEntity=ImageProduct::class, mappedBy="product", orphanRemoval=true, cascade={"persist", "remove"})
     * @Groups({"product:read", "product:write", "subProd:read"})
     */
    private $imageProducts;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"product:read", "subProd:read"})
     */
    private $createdAt;

    private $file;

    public function __construct()
    {
        $this->imageProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getSubCategory(): ?SubCategory
    {
        return $this->subCategory;
    }

    public function setSubCategory(?SubCategory $subCategory): self
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    public function getSeller(): ?Seller
    {
        return $this->seller;
    }

    public function setSeller(?Seller $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * @return Collection|ImageProduct[]
     */
    public function getImageProducts(): Collection
    {
        return $this->imageProducts;
    }

    public function addImageProduct(ImageProduct $imageProduct): self
    {
        if (!$this->imageProducts->contains($imageProduct)) {
            $this->imageProducts[] = $imageProduct;
            $imageProduct->setProduct($this);
        }

        return $this;
    }

    public function removeImageProduct(ImageProduct $imageProduct): self
    {
        if ($this->imageProducts->contains($imageProduct)) {
            $this->imageProducts->removeElement($imageProduct);
            // set the owning side to null (unless already changed)
            if ($imageProduct->getProduct() === $this) {
                $imageProduct->setProduct(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
}
