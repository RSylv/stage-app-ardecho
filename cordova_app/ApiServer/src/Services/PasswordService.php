<?php

namespace App\Services;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class PasswordService
{

    private $encoder;

    /**
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param object $entity
     * @param string $password
     * @return string
     */
    public function encode(object $entity, string $password): string
    {
        return $this->encoder->encodePassword($entity, $password);
    }

    /**
     * @param object $entity
     * @param string $password
     * @return boolean
     */
    public function isValid(object $entity, string $password): bool
    {
        return $this->encoder->isPasswordValid($entity, $password);
    }
}