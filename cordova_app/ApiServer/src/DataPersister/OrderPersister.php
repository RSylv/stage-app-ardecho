<?php

namespace App\DataPersister;

use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;

class OrderPersister implements DataPersisterInterface
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function supports($data): bool 
    {
        return $data instanceof Order;
    }

    public function persist($data)
    {
        $data->setCreatedAt(new \DateTime);
        $data->setStatus("IN_PROGRESS");
        $data->setReference(uniqid("REF_", true));

        $this->em->persist($data);
        $this->em->flush();
    }

    public function remove($data)
    {
        $this->em->remove($data);
        $this->em->flush();
    }
}