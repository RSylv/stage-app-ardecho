<?php

namespace App\DataPersister;

use App\Entity\Product;
use App\Service\ImagesUploadService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\ImageProduct;

final class ProductPersister implements DataPersisterInterface
{

    public function __construct(EntityManagerInterface $em, ImagesUploadService $imageService)
    {
        $this->em = $em;
        $this->imageService = $imageService;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Product;
    }

    public function persist($data, array $context = [])
    {

        // $imgs = $data->get('imageProducts');
        // dd($imgs);

        // if ($imgs->count() > 5) {
        //     # Error (too much files..)
        // } else {

        //     $tabImgs = [];

        //     foreach ($imgs as $im) {
        //         $im = ['name' => $im->getName(), 'size' => $im->getSize(), 'type' => $im->getType()];
        //         array_push($tabImgs, $im);
        //     }

        //     dd($imgs);
        // }


        if (
            $data instanceof Product && (
                ($context['collection_operation_name'] ?? null) === 'post' ||
                ($context['graphql_operation_name'] ?? null) === 'create')
        ) {

            $data->setCreatedAt(new \DateTime());

            // $this->upload($imgs);

            $this->em->persist($data);
            $this->em->flush();
        }
        return $data;
    }

    public function remove($data, array $context = [])
    {
        if (
            $data instanceof Product && (
                ($context['collection_operation_name'] ?? null) === 'delete')
        ) {
            $this->em->remove($data);
        }
    }

    public function upload($imgs)
    {
        // the file property can be empty if the field is not required
        if (null === $imgs) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $imgs->move(
            $imgs->getUploadRootDir(),
            $imgs->getName()
        );

        // set the path property to the filename where you've saved the file
        $this->path = $imgs->getName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

}
