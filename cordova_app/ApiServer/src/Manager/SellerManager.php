<?php

namespace App\Manager;

use App\Entity\Seller;
use App\Services\PasswordService;
use App\Repository\SellerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SellerManager
{
    protected $passwordService;

    protected $em;

    protected $sellerRepository;


    public function __construct(
        EntityManagerInterface $em,
        SellerRepository $sellerRepository,
        PasswordService $passwordService
    ) {
        $this->passwordService = $passwordService;
        $this->sellerRepository = $sellerRepository;
        $this->em = $em;
    }

    public function findEmail(string $email)
    {
        $seller = $this->sellerRepository->findOneByEmail($email);

        return $seller ? $seller->getEmail() : false;
    }

    public function registerAccount(Seller $seller)
    {
        if ($this->findEmail($seller->getEmail())) {
            throw new BadRequestHttpException("Cette adresse email existe deja !");
        } else {
            $pass = $this->passwordService->encode($seller, $seller->getPassword());
            $seller->setPassword($pass);
            $seller->setRoles(["ROLE_SELLER"]);
            $seller->setAccountType("FREE");
            $seller->setIsValidate(false);
            $seller->setCreatedAt(new \DateTime);

            $this->em->persist($seller);
            $this->em->flush();
        }
    }

    public function remove(Seller $seller)
    {
        $this->em->remove($seller);
        $this->em->flush();
    }
}
