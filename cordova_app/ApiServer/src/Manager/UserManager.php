<?php

namespace App\Manager;

use App\Entity\User;
use App\Services\PasswordService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserManager
{
    protected $passwordService;

    protected $em;

    protected $userRepository;


    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository,
        PasswordService $passwordService
    ) {
        $this->passwordService = $passwordService;
        $this->userRepository = $userRepository;
        $this->em = $em;
    }

    public function findEmail(string $email)
    {
        $user = $this->userRepository->findOneByEmail($email);

        return $user ? $user->getEmail() : false;
    }

    public function registerAccount(User $user)
    {
        if ($this->findEmail($user->getEmail())) {
            throw new BadRequestHttpException("user already exists");
        } else {
            $pass = $this->passwordService->encode($user, $user->getPassword());
            $user->setPassword($pass);
            $user->setRoles(["ROLE_USER"]);
            $user->setAccountType("FREE");
            $user->setIsBlocked(true);
            $user->setCreatedAt(new \DateTime);

            $this->em->persist($user);
            $this->em->flush();
        }
    }

    public function remove(User $user)
    {
        $this->em->remove($user);
        $this->em->flush();
    }
}