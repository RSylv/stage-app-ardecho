### Information :

>--------------------------------------------------------------------------------------------------------------------------------------

# Controller\Api : 
-   Classes avec fonctions particulières pour l'api :
    les methodes de base sont gerer par api-platform avec l'annotations @ApiResource.
    Celles-ci permettent de gerer la récuperation de ressources, avec des routes personnalisées. 

# DataPersister : 
-   Permet de gerer, sans renommer les routes, les methodes 'post' des entités.
    Il suffit de créer un fichier PHP, avec le nom de l'entité suivi de 'Persister'.
    La classs porte le nom du fichier, et doit implementer la 'DataPersisterInterface'.
    Les methodes a integrer de base sont :

    - support() 
    - persist()
    - remove()

# Entity :
-   Les entités du projet (Classes virtualisant les tables de la bdd).
    Ici on gere l'api-platform via les annotations sur les classes.
    On peut deployer une entité (ex: User) avec les methodes "get, put, patch, delete", en annotant seulement '@ApiResource'.


# Event :
-   Le dossier Event contient le listener du succès d'authentification.
    Cette classe 'AuthenticationSuccessListener', et ses methodes, permettent l'envoi de données additionnelles au token recu, lors du succès d'authentification.


# Manager :
-   Le dossier manager contient la logique de la création du compte 'User' ou 'Seller'.
    Les manager crées sont appelés par le controller 'CreateAccount.php'.


# Repository : 
-   Class de gestion, par entités, des requetes SQL sous forme de methodes.

# Services :
-   Contient les services utiles au projet.
    Ici le password service (utilisé par le manager lors de la création du compte), et le service de gestion des images.

# Swagger :
-   Contient l'integration a la documentation visuelle de l'api (gerer par swaggerUi), de routes supplementaires non prises en compte par  l'api ressource.
    Ici la Class 'SwaggerDecorator' permet l'ajout du JWT, et donc permet de pouvoir s'authentifier directement via l'interface de swagger,
    a l'adresse : https://localhost:8000/api.




### Pour le reste :

- l'essentiel se situe dans le dossier 'config'.
    On y retrouve les fichier yml liés a la configuration de plusieurs pack :
    
    -> api-platform     ( Configuration du comportement de l'api-platform, et swaggerUi )
    -> lexik_jwt        ( Authentification via le JWT pack de lexik. )
    -> security         ( Gestion des auth, du parefeu, ... )
    -> services         ( Déclaration des services, leur routes, et autres options. )
    -> nelmio_cors      ( CORS, règles/restrictions d'accés a l'adresse 'apiserver'. )



-   Les elements 'sensibles' se trouvent dans le  fichier .env.local a la racine du projet.
    Accés  (bdd, cors et jwt)