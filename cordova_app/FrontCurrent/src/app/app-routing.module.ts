import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriesComponent } from './component/categories/categories.component';
import { CategoryComponent } from './component/category/category.component';
import { SubCategoryComponent } from './component/sub-category/sub-category.component';
import { ProductComponent } from './component/product/product.component';
import { SellerComponent } from './component/seller/seller.component';
import { ProfilUserComponent } from './component/profil-user/profil-user.component';
import { ProfilSellerComponent } from './component/profil-seller/profil-seller.component';
import { MapComponent } from './component/map/map.component';
import { LoginComponent } from './shared/component/login/login.component';
import { RegisterComponent } from './shared/component/register/register.component';
import { AuthGuard } from './guard/guard.guard';


const routes: Routes = [
  { path: 'connexion', component: LoginComponent },
  { path: 'inscription', component: RegisterComponent },
  { path:'', component: CategoriesComponent, data: { animation:"isLeft"} },
  { path:'categorie/:id', component: CategoryComponent, data: { animation:"isLeft"} },
  { path:'sous-categorie/:id', component: SubCategoryComponent, data: { animation:"isRight"} },
  { path:'produit/:id', component: ProductComponent, data: { animation:"isRight"} },
  { path:'vendeur/:id', component: SellerComponent, data: { animation:"isRight"} },
  { path:'profil/acheteur/:id', component: ProfilUserComponent, canActivate: [AuthGuard], data: { animation:"isRight"} },
  { path:'profil/vendeur/:id', component: ProfilSellerComponent, data: { animation:"isRight"} },
  { path:'map', component: MapComponent, canActivate: [AuthGuard], data: { animation:"isRight"} },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }