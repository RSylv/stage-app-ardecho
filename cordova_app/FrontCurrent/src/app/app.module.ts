import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './shared/modules/material/material.module';
import { AppRoutingModule } from './app-routing.module';

import { ApiService } from './services/api.service';
import { ProductPipe } from './pipes/product.pipe';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/component/header/header.component';
import { SidenavComponent } from './shared/component/sidenav/sidenav.component';
import { CategorySidenavComponent } from './shared/component/sidenav/category-sidenav/category-sidenav.component';
import { FooterComponent } from './shared/component/footer/footer.component';
import { CartComponent } from './shared/component/cart/cart.component';
import { LoginComponent } from './shared/component/login/login.component';
import { RegisterComponent } from './shared/component/register/register.component';
import { CategoryComponent } from './component/category/category.component';
import { SubCategoryComponent } from './component/sub-category/sub-category.component';
import { ProductComponent } from './component/product/product.component';
import { ProfilUserComponent } from './component/profil-user/profil-user.component';
import { ProfilSellerComponent } from './component/profil-seller/profil-seller.component';
import { MapComponent } from './component/map/map.component';
import { PromoteCategoryComponent } from './shared/component/promote-category/promote-category.component';
import { PromoteSellerComponent } from './shared/component/promote-seller/promote-seller.component';
import { PromoteProductComponent } from './shared/component/promote-product/promote-product.component';
import { SellerComponent } from './component/seller/seller.component';
import { SellerProductComponent } from './component/seller/seller-product/seller-product.component';
import { CategoriesComponent } from './component/categories/categories.component';
import { PromoteSubCategoryComponent } from './shared/component/promote-sub-category/promote-sub-category.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialFileInputModule } from 'ngx-material-file-input';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    CartComponent,
    LoginComponent,
    RegisterComponent,
    CategoryComponent,
    SubCategoryComponent,
    ProductComponent,
    ProfilUserComponent,
    ProfilSellerComponent,
    MapComponent,
    ProductPipe,
    PromoteCategoryComponent,
    PromoteSellerComponent,
    PromoteProductComponent,
    SellerComponent,
    SellerProductComponent,
    CategorySidenavComponent,
    CategoriesComponent,
    PromoteSubCategoryComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    MaterialFileInputModule,
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
