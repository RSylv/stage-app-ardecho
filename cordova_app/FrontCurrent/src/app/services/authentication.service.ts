import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from './api.service';
import { Utilisateur } from 'src/app/model/utilisateur.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<Utilisateur>;
  public currentUser: Observable<Utilisateur>;

  constructor(private apiService: ApiService, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Utilisateur>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Utilisateur {
    return this.currentUserSubject.value;
  }

  public isConnected() {
    return localStorage.getItem('currentUser') !== null;
  }

  login(email: string, password: string) {

    let data = {
      'email': email,
      'password': password
    };

    // On envoi les donnees a l'Api :
    this.apiService.login(data).subscribe(
      user => {

        // On sauvegarde la reponse (JWT et Infos Utilisateur) dans le local storage,
        // pour garder l'utilisateur connecté pendant la navigation :
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);

        window.location.reload();        
      }

    );
  }

  logout() {
    // On supprime l'utilsateur du localStorage et on met a null le currentUser
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }  

}
