import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { Category } from "../model/category.model";
import { SubCategory } from "../Model/sub-category.model";
import { Product } from "../model/product.model";
import { Seller } from "../model/seller.model";
import { User } from "../model/user.model";
import { Utilisateur } from '../model/utilisateur.model';

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient) { }

  baseUrl: string = environment.api_server;
  serverUrl: string = environment.url_server;
  url_auth: string = environment.api_authentified_url;
  format: string = ".json";

  // LOGIN 
  login(user: {}): Observable<Utilisateur> {
    return this.http.post<any>(this.serverUrl + '/authentication_token', user)
  }


  // USERS
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + "/users" + this.format);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + "/users/" + id + this.format);
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl + "/users/create", user);
  }



  // SELLERS
  getSellers(): Observable<Seller[]> {
    return this.http.get<Seller[]>(this.baseUrl + "/sellers" + this.format);
  }

  getSellerById(id: number): Observable<Seller> {
    return this.http.get<Seller>(this.baseUrl + "/sellers/" + id + this.format);
  }

  createSeller(Seller: Seller): Observable<Seller> {
    return this.http.post<Seller>(this.baseUrl + "/sellers/create", Seller);
  }



  // CATEGORIES
  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(
      this.baseUrl + "/categories" + this.format
    );
  }

  getCategoryById(id: number): Observable<Category> {
    return this.http.get<Category>(
      this.baseUrl + "/categories/" + id + this.format
    );
  }

  getSubcategoriesFromCategory(id: number): Observable<SubCategory[]> {
    return this.http.get<SubCategory[]>(
      this.baseUrl + "/category/" + id + "/sub_categories"
    );
  }


  // SUBCATEGORIES
  getSubCategories(id: number): Observable<SubCategory> {
    return this.http.get<SubCategory>(
      this.baseUrl + "/sub_categories/" + id + this.format
    );
  }

  getProductFromSubCategory(id: number): Observable<SubCategory[]> {
    return this.http.get<SubCategory[]>(
      this.baseUrl + "/sub_categories/" + id + "/products"
    );
  }


  // PRODUCTS
  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + '/products' + this.format)
  }

  createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.url_auth + '/products', product)
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(
      this.baseUrl + "/products/" + id
    );
  }
}
