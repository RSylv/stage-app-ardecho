import { Component, OnInit } from '@angular/core';
import { Category } from "src/app/model/category.model";
import { ApiService } from "src/app/services/api.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Category[] = [];
  category: Category;
  slidesCategory = []; 

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.api.getCategories().subscribe(
      (data) => {
        this.categories = data;
        this.getCarrousel();
      },
      (error) => {
        console.log(error.status, error.statusText);
      }
    );
  }

  getCarrousel() {
    this.slidesCategory = [];
    for (let i = 0; i < this.categories.length; i++) {
      this.slidesCategory.push({ image: 'https://via.placeholder.com/1550x580.png?text=' + this.categories[i].name });
    }
  }
  
}
