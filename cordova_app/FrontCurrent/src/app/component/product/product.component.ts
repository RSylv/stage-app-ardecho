import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Product } from 'src/app/model/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  product: Product;
  constructor(private route: ActivatedRoute, private api: ApiService) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.getProduct(routeParams.id);
    });
  }

  getProduct(id: number) {
    this.api.getProduct(id).subscribe(
      (data) => {
        this.product = data;
      },
      (error) => {
        console.log(error.status, error.statusText);
      }
    );
  }
}
