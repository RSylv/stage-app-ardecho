import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/model/category.model';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  category: Category;
  slidesSubCategory = [];

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.getCategoryById(routeParams.id);
    });
  }

  getCategoryById(id: number) {
    this.api.getCategoryById(id).subscribe(
      (data) => {
        this.category = data;
        this.getCarrouselSubCategory();
      },
      (error) => {
        console.log(error.status, error.statusText);
      }
    );
  }

  getCarrouselSubCategory() {
    this.slidesSubCategory = [];
    for (let i = 0; i < this.category.subCategories.length; i++) {
      this.slidesSubCategory.push({image: 'https://via.placeholder.com/1550x580.png?text=' + this.category.subCategories[i].name });
    }
  }
}