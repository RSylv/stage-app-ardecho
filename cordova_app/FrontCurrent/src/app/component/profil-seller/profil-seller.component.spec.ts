import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilSellerComponent } from './profil-seller.component';

describe('ProfilSellerComponent', () => {
  let component: ProfilSellerComponent;
  let fixture: ComponentFixture<ProfilSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
