import { Component, OnInit } from '@angular/core';
import { Seller } from 'src/app/model/seller.model';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { FileValidator } from 'ngx-material-file-input';
import { Product } from 'src/app/model/product.model';
import { ImageProduct } from 'src/app/model/imageProduct.model';
import { Category } from 'src/app/model/category.model';
import { SubCategory } from 'src/app/Model/sub-category.model';


@Component({
  selector: 'app-profil-seller',
  templateUrl: './profil-seller.component.html',
  styleUrls: ['./profil-seller.component.scss']
})
export class ProfilSellerComponent implements OnInit {

  seller: Seller;
  categories: Category[] = [];
  subCategories: SubCategory[] = [];

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  /**
   * 100 MB (=100 * 2 ** 20) = 104857600
   * Ici 0.7MB (pour 5 images max)
   * Ou faire en sorte d'ajouter image par image, avec 0.3MB autorisé pour chaque image :
   */
  readonly maxSize = 734003;

  constructor(private apiService: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.seller = JSON.parse(localStorage.getItem('currentUser')).data;

    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required],
    });
    this.secondFormGroup = this.formBuilder.group({
      subCategory: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.min(0)],
      stock: [0, Validators.min(0)],
    });
    this.thirdFormGroup = this.formBuilder.group({
      imageProducts: [
        undefined,
        [Validators.required, FileValidator.maxContentSize(this.maxSize)]
      ]
    });
    this.getCategories();
  }

  // Récupération de toutes les catégories :
  getCategories() {
    this.apiService.getCategories().subscribe((data) => {
      this.categories = data;
    }, (error) => {
      throw new Error((error.status, error.statusText));
    });
  }

  // Récupération de toutes les sous-catégories :
  getSubCategories(id: number) {
    this.apiService.getSubcategoriesFromCategory(id).subscribe((data) => {
      this.subCategories = data;
    }, (error) => {
      console.log(error.status, error.statusText);
    });
  }

  // Envoi Du Form pour l'enregistrement d'un produit:
  submitForm() {

    // Si le form n'est pas Valid (selon les validators), on sort :
    if (this.secondFormGroup.invalid) {
      return;
    }

    // Creation des routes de l'Api, pour les relations 'SubCategory' et 'Seller' : 
    const subCategory = "/api/sub_categories/" + this.secondFormGroup.controls.subCategory.value;
    const seller = "/api/sellers/" + this.seller.id;

    // Récupération des images :
    let images = this.thirdFormGroup.controls.imageProducts.value._files;
    let names = [];

    // On cree un tableau d'objects 'Image' :
    for (let i = 0; i < images.length; i++) {
      names.push(new ImageProduct(images[i].name, images[i].size, images[i].type))
    }

    // Création d'un nouvel object 'Produit' :
    let product = new Product(
      this.secondFormGroup.controls.title.value,
      this.secondFormGroup.controls.description.value,
      this.secondFormGroup.controls.price.value,
      this.secondFormGroup.controls.stock.value,
      subCategory,
      seller,
      names
    );


    // On envoi le produit a l'api :
    this.apiService.createProduct(product).subscribe((data => {
      // console.log(data);
    }));

  }

}
