import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubCategory } from 'src/app/model/sub-category.model';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {

  subCategory: SubCategory;
  subCategoryAllProduct: SubCategory[] = [];
  sellerSubCategory = [];
  nbSellerView: number = 0;

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.getSubCategories(routeParams.id);
      this.getProductFromSubCategory(routeParams.id);
    });
  }

  getSubCategories(id: number) {
    this.api.getSubCategories(id).subscribe(
      (data) => {
        this.subCategory = data;
      },
      (error) => {
        (error.status, error.statusText);
      }
    );
  }

  getProductFromSubCategory(id: number) {
    this.api.getProductFromSubCategory(id).subscribe(
      (data) => {
        this.subCategoryAllProduct = data;        
        this.getSellerSubCategory();
      },
      (error) => {
        console.log(error.status, error.statusText);
      }
    );
  }

  getSellerSubCategory() {
    this.sellerSubCategory = [];

    for (let i = 0; i < this.subCategoryAllProduct.length; i++) {
      this.sellerSubCategory.push(this.subCategoryAllProduct[i].seller);
    }

  }
}