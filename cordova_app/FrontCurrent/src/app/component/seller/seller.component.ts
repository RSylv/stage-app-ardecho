import { Component, OnInit } from '@angular/core';
import { Seller } from 'src/app/model/seller.model';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.scss']
})
export class SellerComponent implements OnInit {

  seller: Seller;

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.getSellerById(routeParams.id);
    });
    
  }
  
  getSellerById(id: number) {
    this.api.getSellerById(id).subscribe(
      (data) => {
        this.seller = data;       
      },
      (error) => {
        console.log(error.status, error.statusText);
      }
    );
  }

}
