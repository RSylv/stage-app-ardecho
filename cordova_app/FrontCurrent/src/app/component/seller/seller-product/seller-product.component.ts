import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/model/product.model';

@Component({
  selector: 'app-seller-product',
  templateUrl: './seller-product.component.html',
  styleUrls: ['./seller-product.component.scss']
})
export class SellerProductComponent implements OnInit {

  @Input() product: Product;

  constructor() { }

  ngOnInit(): void {
    
  }

}
