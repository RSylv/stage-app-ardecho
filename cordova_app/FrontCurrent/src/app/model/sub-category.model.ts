export class SubCategory {
    id: number;
    name: string;
    category: string;
    seller: {
        id: number,
        companyName: string,
        name: string,
        surname: string,
        address: string,
        zipCode: string,
        city: string,
        openingHours: string,
        email: string,
        phone: string,
        createdAt: string,
    };
    
    constructor(name: string = '', category: string = '') {

        this.name = name;
        this.category = category;        
    }
}
