export class Seller {
    
    id: number;
    email: string;
    roles: string[];
    password: string;
    companyName: string;
    siret: string;
    phone: string;
    name: string;
    surname: string;
    address: string;
    city: string;
    zipCode: string;
    openingHours: string;
    isValidate: string;
    accountType: string;
    createdAt: string;
    products: [];
    
    constructor(email: string = '', password: string = '', companyName: string = '', siret: string = '', phone: string = '', name: string = '', surname: string = '', address: string = '', city: string = '', zipCode: string, openingHours: string = '')
    {
        this.email = email;
        this.password = password;
        this.companyName = companyName;
        this.siret = siret;
        this.phone = phone;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.openingHours = openingHours;
    }
}
