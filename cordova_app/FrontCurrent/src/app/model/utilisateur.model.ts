import { User } from './user.model';
import { Seller } from './seller.model';


export interface Utilisateur {
  email: string;
  password: string;
  token: string;
  data: User | Seller;
}