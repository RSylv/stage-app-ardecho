export class User {
    
    id: number;
    email: string;
    roles: string[];
    password: string;
    name: string;
    surname: string;
    phone: string;
    address: string;
    city: string;
    zipCode: string;
    accountType: string;
    isBlocked: string;
    createdAt: string;

    constructor(email: string = '', password: string = '', name: string = '', surname: string = '', phone: string = '', address: string = '', city: string = '', zipCode: string = '') 
    { 
        email = this.email;
        password = this.password;
        name = this.name;
        surname = this.surname;
        phone = this.phone;
        address = this.address;
        city = this.city;
        zipCode = this.zipCode;
    }
}
