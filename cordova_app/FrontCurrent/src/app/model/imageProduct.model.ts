export class ImageProduct
{
    id: number;
    name: [];
    size: number;
    type: string;
    product: string;

    constructor(name: [] = [], size: number = 0, type: string = '') {        
        this.name = name;
        this.size = size;
        this.type = type;
    }
}