export class Category {
    id: number;
    name: string;
    subCategories: [
        {
            id: number,
            name: string,
        }
    ]

    constructor(name: string = '') {
        
        this.name = name;
    }
}
