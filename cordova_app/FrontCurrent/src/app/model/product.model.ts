export class Product
{
    id: number;
    title: string;
    description: string;
    price: string;
    stock: number;
    subCategory: string;
    seller: string;
    imageProduct: any;

    constructor(title: string = '', description: string = '', price: string = '', stock: number = 0, subCategory: string = '', seller: string = '', imageProduct: any = [{}])
    {
        this.title = title;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.subCategory = subCategory;
        this.seller = seller;
        this.imageProduct = imageProduct;
    }
}
