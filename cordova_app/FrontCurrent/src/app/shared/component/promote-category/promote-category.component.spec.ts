import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoteCategoryComponent } from './promote-category.component';

describe('PromoteCategoryComponent', () => {
  let component: PromoteCategoryComponent;
  let fixture: ComponentFixture<PromoteCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoteCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoteCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
