import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/model/category.model';

@Component({
  selector: 'app-promote-category',
  templateUrl: './promote-category.component.html',
  styleUrls: ['./promote-category.component.scss']
})
export class PromoteCategoryComponent implements OnInit {

  @Input() category: Category;

  constructor() { }

  ngOnInit(): void {
   
  }
}
