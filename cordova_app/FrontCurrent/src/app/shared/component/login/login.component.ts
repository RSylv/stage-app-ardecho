import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Utilisateur } from 'src/app/model/utilisateur.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted = false;
  loading = false;
  returnUrl: string;
  hide = true;
  currentUser: Utilisateur;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
  ) {
    this.currentUser = this.authenticationService.currentUserValue
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

  }

  seConnecter() {

    this.isSubmitted = true;

    // Si le form est invalide :
    if (this.loginForm.invalid) {
      this.isSubmitted = false;
      return;
    }

    this.loading = true;
    this.authenticationService.login(
      this.loginForm.controls.email.value,
      this.loginForm.controls.password.value,
    )

  }

}
