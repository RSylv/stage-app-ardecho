import { Component, OnInit, Input } from '@angular/core';
import { Seller } from 'src/app/model/seller.model';

@Component({
  selector: 'app-promote-seller',
  templateUrl: './promote-seller.component.html',
  styleUrls: ['./promote-seller.component.scss']
})
export class PromoteSellerComponent implements OnInit {

  @Input() seller: Seller;
  
  constructor() { }

  ngOnInit(): void {
  }

}
