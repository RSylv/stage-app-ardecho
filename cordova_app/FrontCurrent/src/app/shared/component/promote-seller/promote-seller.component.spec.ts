import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoteSellerComponent } from './promote-seller.component';

describe('PromoteSellerComponent', () => {
  let component: PromoteSellerComponent;
  let fixture: ComponentFixture<PromoteSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoteSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoteSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
