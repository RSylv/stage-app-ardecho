import { Component, OnInit } from '@angular/core';
import { Category } from "src/app/model/category.model";
import { ApiService } from "src/app/services/api.service";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  categories: Category[] = [];
  category: Category;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.apiService.getCategories().subscribe(
      (data) => {
        this.categories = data;
      },
      (error) => {
        console.log(error.status, error.statusText);
      }
    );
  }
}
