import { Component, OnInit, Input } from '@angular/core';
import { Category } from "src/app/model/category.model";

@Component({
  selector: 'app-category-sidenav',
  templateUrl: './category-sidenav.component.html',
  styleUrls: ['./category-sidenav.component.scss'],
})
export class CategorySidenavComponent implements OnInit {

  panelOpenState = false;
  @Input() category: Category;

  constructor() { }

  ngOnInit(): void {
  }

}
