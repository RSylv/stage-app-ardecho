import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/model/product.model';


@Component({
  selector: 'app-promote-product',
  templateUrl: './promote-product.component.html',
  styleUrls: ['./promote-product.component.scss']
})
export class PromoteProductComponent implements OnInit {

  @Input() product: Product;
  
  constructor() { }

  ngOnInit(): void {
  }

}
