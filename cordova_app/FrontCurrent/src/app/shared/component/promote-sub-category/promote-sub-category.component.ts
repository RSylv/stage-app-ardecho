import { Component, OnInit, Input } from '@angular/core';
import { SubCategory } from 'src/app/Model/sub-category.model';

@Component({
  selector: 'app-promote-sub-category',
  templateUrl: './promote-sub-category.component.html',
  styleUrls: ['./promote-sub-category.component.scss']
})
export class PromoteSubCategoryComponent implements OnInit {

  @Input() sub: SubCategory;

  constructor() { }

  ngOnInit(): void {
  }

}
