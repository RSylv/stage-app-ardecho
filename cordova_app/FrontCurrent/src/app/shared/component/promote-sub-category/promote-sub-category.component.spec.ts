import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoteSubCategoryComponent } from './promote-sub-category.component';

describe('PromoteSubCategoryComponent', () => {
  let component: PromoteSubCategoryComponent;
  let fixture: ComponentFixture<PromoteSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoteSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoteSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
