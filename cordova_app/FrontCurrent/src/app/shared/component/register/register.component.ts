import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Seller } from 'src/app/model/seller.model';
import { User } from 'src/app/model/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  isBuyer: boolean = null;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  constructor(private apiService: ApiService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.firstFormGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  secondPartForm() {
    if (!this.isBuyer) {
      this.secondFormGroup = this.formBuilder.group({
        companyName: ['', Validators.required],
        siret: ['', Validators.required],
        phone: ['', Validators.required],
        name: ['', Validators.required],
        surname: ['', Validators.required],
      });
      this.thirdFormGroup = this.formBuilder.group({
        address: ['', Validators.required],
        city: ['', Validators.required],
        zipCode: ['', Validators.required],
        openingHours: ['', Validators.required],
      });
    }
    else if (this.isBuyer) {
      this.secondFormGroup = this.formBuilder.group({
        phone: ['', Validators.required],
        name: ['', Validators.required],
        surname: ['', Validators.required],
      });
      this.thirdFormGroup = this.formBuilder.group({
        address: ['', Validators.required],
        city: ['', Validators.required],
        zipCode: ['', Validators.required],
      });
    }
  }

  submitForm() {

    // Si le form n'est pas Valid (selon les validators), on sort :
    if (
      this.firstFormGroup.invalid
      || this.secondFormGroup.invalid
      || this.thirdFormGroup.invalid
    ) {
      console.log("Form Invalid");
      return;
    }

    let data: any = {};

    if (this.isBuyer)
    {
      data = new User(
        this.firstFormGroup.controls.email.value,
        this.firstFormGroup.controls.password.value,
        this.secondFormGroup.controls.name.value,
        this.secondFormGroup.controls.surname.value,
        this.secondFormGroup.controls.phone.value,
        this.thirdFormGroup.controls.address.value,
        this.thirdFormGroup.controls.city.value,
        this.thirdFormGroup.controls.zipCode.value
      );
    }
    else if (!this.isBuyer)
    {
      data = new Seller(
        this.firstFormGroup.controls.email.value,
        this.firstFormGroup.controls.password.value,
        this.secondFormGroup.controls.companyName.value,
        this.secondFormGroup.controls.siret.value,
        this.secondFormGroup.controls.phone.value,
        this.secondFormGroup.controls.name.value,
        this.secondFormGroup.controls.surname.value,
        this.thirdFormGroup.controls.address.value,
        this.thirdFormGroup.controls.city.value,
        this.thirdFormGroup.controls.zipCode.value,
        this.thirdFormGroup.controls.openingHours.value,
      );
      // admin@adsqcmin.fr // admin@admin.fr
    }
    

    console.log(data);

    this.apiService.createSeller(data).subscribe(data => {
      console.log(data);
    }, error => console.log(error));

    this.router.navigateByUrl('/connexion');

  }

}
