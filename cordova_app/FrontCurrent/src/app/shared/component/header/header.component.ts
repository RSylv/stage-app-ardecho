import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLogged: boolean;

  @Output() toggleSidenavForMe: EventEmitter<any> = new EventEmitter();

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.isLogged = this.auth.isConnected(); 

    // Si l'user est conecté, onredirige sur la page 'home' :
    if (this.isLogged) {
      this.router.navigate(['/']);
    }
  }

  toggleSidenav() {
    this.toggleSidenavForMe.emit();
  }

  checkLogin() {
    if (this.auth.isConnected()) {
      let id = JSON.parse(localStorage.getItem('currentUser')).data.id;

      if (JSON.parse(localStorage.getItem('currentUser')).data.companyName) {
        this.router.navigate(['/profil/vendeur/', id]);     
      }
      else {
        this.router.navigate(['/profil/acheteur/', id]);           
      }

    } else {
      this.router.navigateByUrl('/connexion');
    }
  }

  deconexion() {
    this.auth.logout();
    window.location.reload();
    // this.router.navigate(['/']);
  }

}
